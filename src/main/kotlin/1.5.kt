import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: D’String a MutableList<Char>
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string = escaner.next()
    println(stringToMutableList(string))
}

fun stringToMutableList(input:String):MutableList<Char>{
    val mutableList = mutableListOf<Char>()
    for(i in input.indices) mutableList.add(input[i])
    return mutableList
}