import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Escriu una línia
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val espais = escaner.nextInt()
    val caracters = escaner.nextInt()
    val caracter = escaner.next().single()
    escriuUnaLinia(espais,caracters,caracter)
}
fun escriuUnaLinia(espais:Int,caracters:Int,caracter:Char){
    for(i in 1..espais) print(" ")
    for(i in 1..caracters) print(caracter)
}