import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Divideix un String
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string = escaner.nextLine()
    val caracter = escaner.next().single()
    println(divideixString(string,caracter))
}

fun divideixString(input:String,divisor:Char):MutableList<String>{
    val mutableList = mutableListOf<String>()
    var element = ""
    for(i in input.indices){
        if(input[i]!=divisor){
            element+=input[i]
        }
        else{
            mutableList.add(element)
            element=""
        }
    }
    mutableList.add(element)
    return mutableList
}