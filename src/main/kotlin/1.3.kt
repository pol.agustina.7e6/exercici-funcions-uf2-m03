import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Caràcter d’un String
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string = escaner.nextLine()
    val posicio = escaner.nextInt()
    caracterString(string,posicio)
}

fun caracterString (input:String,inputVal:Int){
    if(inputVal in input.indices) println(input[inputVal])
    else println("La mida de l'String és inferior a $inputVal")
}