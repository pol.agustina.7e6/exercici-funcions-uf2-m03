import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Escriu un marc per un String
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string = escaner.nextLine()
    val caracter = escaner.next().single()
    marc(string,caracter)
}

fun marc (input:String,caracter:Char){
    for(i in 0..4){
        when(i){
            0,4 -> for(j in 0 until input.length+4) print(caracter)
            1,3 -> for(j in 0 until input.length+4) {
                if(j==0 || j==input.length+3) print(caracter)
                else print(" ")
            }
            else -> print("$caracter $input $caracter")
        }
        println()
    }

}