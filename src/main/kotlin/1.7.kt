import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Eleva’l
*/

fun main(){
    val escaner = Scanner(System.`in`)
    val numero1 = escaner.nextInt()
    val numero2 = escaner.nextInt()
    println(eleval(numero1,numero2))
}

fun eleval(val1:Int,val2:Int):Int{
    var resultat=1
    for (i in 1..val2) resultat*=val1
    return resultat
}