import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: String és igual
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string1 = escaner.next()
    val string2 = escaner.next()
    println(stingsIguals(string1,string2))
}

fun stingsIguals (input1:String,input2:String):Boolean{
    var iguals = true
    var i = 0
    while (i in input1.indices&&i in input2.indices&&iguals){
        if(input1[i]!=input2[i])iguals = false
        i++
    }
    return iguals
}