import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Escriu una creu
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val tamany = escaner.nextInt()
    val caracter = escaner.next().single()
    creu(tamany,caracter)
}

fun creu (tamany:Int = 3,caracter:Char){
    for(i in 0 until tamany){
        if(i!=tamany/2) {
            for(j in 1..tamany/2) print(" ")
            print(caracter)
        }
        else {
            for(j in 1..tamany) print(caracter)
        }
        println()
    }
}