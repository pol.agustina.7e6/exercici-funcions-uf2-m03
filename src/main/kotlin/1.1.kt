import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Mida d’un String
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string = escaner.next()
    println(midaString(string))
}

fun midaString (input:String):Int{
    var mida = 0
    for (i in input.indices) mida++
    return mida
}