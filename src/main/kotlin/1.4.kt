import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 07/12/2022
* TITLE: Subseqüència
*/

fun main (){
    val escaner = Scanner(System.`in`)
    val string = escaner.nextLine()
    val start = escaner.nextInt()
    val end = escaner.nextInt()
    subsequencia(string,start,end)
}

fun subsequencia (input:String,val1:Int,val2:Int){
    if(val1 in input.indices && val2 in input.indices){
        for (i in val1 until  val2) print(input[i])
    }
    else println("La subseqüència $val1-$val2 de l'String no existeix")
}